$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    //khai báo tiêu đề table tương ứng các thuộc tính của đối tượng Order
    var gTitle = ["STT", "orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "hoTen", "soDienThoai", "diaChi", "trangThai", "action"];
    //biến trạng thái
    var gDEFAULT_STATUS = 0;
    var gCURRENT_STATUS = 0;
    var gADD_STATUS = 1;
    var gEDIT_STATUS = 2;
    var gDEL_STATUS = 3;
    //dữ liệu cho bảng table data
    const gCOLUMN_STT = 0;
    const gCOLUMN_ORDER_ID = 1;
    const gCOLUMN_COMBO_SIZE = 2;
    const gCOLUMN_PIZZA_TYPE = 3;
    const gCOLUMN_DRINK = 4;
    const gCOLUMN_NAME = 5;
    const gCOLUMN_PHONE = 6;
    const gCOLUMN_ADDRESS = 7;
    const gCOLUMN_STATUS = 8;
    const gCOLUMN_ACTION = 9;
    //biến id
    var gOrderId;
    //biến lưu danh sách đồ uống
    var gDrinkObj = null;
    //khai báo biến lưu dữ liệu nhận từ server
    var gOrderObj = {
        orderArr: [],
        //add new order
        addNewOrder: function (paramOrderData) {
            if (gCURRENT_STATUS == gADD_STATUS) {
                this.orderArr.unshift(paramOrderData);
            }
            if (gCURRENT_STATUS == gEDIT_STATUS) {
                var vOrderIndex = this.getIndexByOrderId(gOrderId);
                this.orderArr.splice(vOrderIndex, 1, paramOrderData);
            }
        },
        //delete current order
        deleteOrderData: function () {
            var vOrderIndex = this.getIndexByOrderId(gOrderId);
            this.orderArr.splice(vOrderIndex, 1);
        },
        // get order index from id
        // input: paramOrderId là id cần tìm index
        // output: trả về chỉ số (index) trong mảng user
        getIndexByOrderId: function (paramOrderId) {
            var vOrderIndex = -1;
            var vOrderFound = false;
            var vLoopIndex = 0;
            while (!vOrderFound && vLoopIndex < this.orderArr.length) {
                if (this.orderArr[vLoopIndex].id === paramOrderId) {
                    vOrderIndex = vLoopIndex;
                    vOrderFound = true;
                }
                else {
                    vLoopIndex++;
                }
            }
            return vOrderIndex;
        },
        // get new id
        getNewId: function () {
            var vOrderIndex = -1;
            var vOrderFound = false;
            var vLoopIndex = 0;
            while (!vOrderFound && vLoopIndex < this.orderArr.length) {
                if (this.orderArr[vLoopIndex].id !== vLoopIndex) {
                    vOrderIndex = vLoopIndex;
                    vOrderFound = true;
                }
                else {
                    vLoopIndex++;
                }
            }
            return vOrderIndex;
        }
    };
    // dùng dữ liệu này để cho vào ô select combo size
    const g_JSON_COMBO_SIZE_DATA = `[ 
        {"combosize": "S", "duongKinh":"20","suonNuong":"2", "saLad":"200", "nuocUong":"2", "thanhTien":"150000"},
        {"combosize": "M", "duongKinh":"25","suonNuong":"4", "saLad":"300", "nuocUong":"3", "thanhTien":"200000"},
        {"combosize": "L", "duongKinh":"30","suonNuong":"8", "saLad":"500", "nuocUong":"4", "thanhTien":"250000"}
      ]`;
    var gCOMBO_SIZE = JSON.parse(g_JSON_COMBO_SIZE_DATA);
    //số thứ tự bảng
    var gSTT = 1;
    // Khởi tạo DataTable, gán data và mapping các columns
    var gTableData = $("#table-data").DataTable({
        columns: [
            { data: gTitle[gCOLUMN_STT] },
            { data: gTitle[gCOLUMN_ORDER_ID] },
            { data: gTitle[gCOLUMN_COMBO_SIZE] },
            { data: gTitle[gCOLUMN_PIZZA_TYPE] },
            { data: gTitle[gCOLUMN_DRINK] },
            { data: gTitle[gCOLUMN_NAME] },
            { data: gTitle[gCOLUMN_PHONE] },
            { data: gTitle[gCOLUMN_ADDRESS] },
            { data: gTitle[gCOLUMN_STATUS] },
            { data: gTitle[gCOLUMN_ACTION] },
        ],
        // định nghĩa lại 02 cột studentId và SubjectId, để mapping
        columnDefs: [
            {
                targets: gCOLUMN_STT,
                render: function () {
                    return gSTT++;
                }
            },
            {
                targets: gCOLUMN_PIZZA_TYPE,
                render: function (data) {
                    if(data == "Seafood")
                    return "Hải sản";
                    if(data == "Hawaii")
                    return "Hawaii";
                    if(data == "Bacon")
                    return "Thịt hun khói";
                    else
                    return data;
                }
            },
            {
                defaultContent: `
          <img class="edit-order" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
          <img class="delete-order" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
        `,
                targets: gCOLUMN_ACTION
            }
        ]
    });

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    //Khi nút filter được ấn
    $("#btn-filter-data").on("click", onButtonFilterDataClick);
    //Khi ấn nút thêm order mới
    $("#btn-add-order").on("click", function () {
        loadDrinkToSelect(gDrinkObj); //load danh sách đồ uống vào select menu
        loadComboToSelect(); //load danh sách combo vào select menu
        $("#order-modal").modal("show"); //hiển thị modal
        gCURRENT_STATUS = gADD_STATUS; //gán trạng thái hiện tại = trạng thái thêm mới
        $("#div-form-mod").html(gCURRENT_STATUS); //hiển thị trạng thái hiện tại
    });
    //Khi nút edit được ấn
    $("#table-data").on("click", ".edit-order", function () {
        gCURRENT_STATUS = gEDIT_STATUS;
        $("#div-form-mod").html(gCURRENT_STATUS);
        loadOrderToModal(this);
    });
    //Khi nút save được ấn
    $("#btn-save-order").on("click", function () {
        onBtnSaveOrderClick();
    });
    //Khi người dùng chọn combo size
    $("#sel-combosize").on("change", function () {
        onSelectComboChange();
    });
    //Khi nút delete được ấn
    $("#table-data").on("click", ".delete-order", function () {
        gCURRENT_STATUS = gDEL_STATUS;
        //Lấy dữ liệu tương ứng dòng được ấn
        var vRowOfBtn = $(this).closest("tr");
        var vOrderOfRow = gTableData.row(vRowOfBtn).data();
        console.log(vOrderOfRow);
        gOrderId = vOrderOfRow.id;
        $("#delete-confirm-modal").modal("show");
    });
    // thêm sự kiện reset data khi ẩn modal form insert/update
    $("#order-modal").on("hidden.bs.modal", function () {
        resetFormData();
    });
    // Thêm sự kiện cho nút xoá
    $("#btn-confirm-delete-order").on("click", function () {
        onIconDeleteUserClick();
    });


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    // hàm xử lý sự kiện load trang
    function onPageLoading() {
        // lấy data từ server danh sách order
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType: 'json',
            async: false,
            success: function (responseObject) {
                gOrderObj.orderArr = responseObject;
                //load Order Arr to table
                loadDataToTable(gOrderObj.orderArr);
            },
            error: function (error) {
                console.assert(error.responseText);
            }
        });
        //lấy data từ server danh sách thức uống
        gDrinkObj = callAPIGetDrinks();
    }

    //Khi nút icon edit được ấn
    function loadOrderToModal(paramIconEditOrder) {

        //Lấy dữ liệu tương ứng dòng được ấn
        var vRowOfBtn = $(paramIconEditOrder).closest("tr");
        var vOrderOfRow = gTableData.row(vRowOfBtn).data();
        console.log(vOrderOfRow);
        //Gán dữ liệu lên form Modal
        gOrderId = vOrderOfRow.id;
        $("#sel-combosize").append($('<option>', { val: vOrderOfRow.kichCo, html: vOrderOfRow.kichCo })).attr("disabled", true);
        $("#sel-combosize option:last").attr("selected", true);
        $("#inp-duongkinh").val(vOrderOfRow.duongKinh);
        $("#inp-suonnuong").val(vOrderOfRow.suon);
        $("#inp-salad").val(vOrderOfRow.salad);
        $("#inp-sluong-nuoc").val(vOrderOfRow.soLuongNuoc);
        $("#inp-thanhtien").val(vOrderOfRow.thanhTien);
        $("#sel-pizzatype").append($('<option>', { val: vOrderOfRow.loaiPizza, html: vOrderOfRow.loaiPizza })).attr("disabled", true);
        $("#sel-pizzatype option:last").attr("selected", true);
        $("#inp-voucherid").val(vOrderOfRow.idVourcher).attr("disabled", true);
        $("#sel-drink").append($('<option>', { val: vOrderOfRow.idLoaiNuocUong, html: vOrderOfRow.idLoaiNuocUong })).attr("disabled", true);
        $("#sel-drink option:last").attr("selected", true);
        $("#inp-hoten").val(vOrderOfRow.hoTen).attr("disabled", true);
        $("#inp-email").val(vOrderOfRow.email).attr("disabled", true);
        $("#inp-sodienthoai").val(vOrderOfRow.soDienThoai).attr("disabled", true);
        $("#inp-diachi").val(vOrderOfRow.diaChi).attr("disabled", true);
        $("#inp-loinhan").val(vOrderOfRow.loiNhan).attr("disabled", true);
        // $("#modal-sel-status").html("");
        $("#modal-sel-status").append($('<option>', { val: vOrderOfRow.trangThai, html: vOrderOfRow.trangThai }));
        $("#modal-sel-status option:last").attr("selected", true);
        loadDrinkToSelect(callAPIGetDrinks()); //load danh sách đồ uống vào select menu
        loadComboToSelect(); //load danh sách combo vào select menu
        $("#order-modal").modal("show");
    }

    // Hàm filer dữ liệu theo điều kiện
    // bạn cần viết theo khung 04 bước xử lý
    function onButtonFilterDataClick() {
        // khai báo đối tượng chứa các tham số filter
        var vOrderFilterData = {
            pizzaType: "",
            status: ""
        };
        //1. thu thập dữ liệu
        getFormData(vOrderFilterData);
        //2. validate
        // bỏ qua bước validate này

        // 3. xử lý nghiệp vụ lọc dữ liệu, tại front-end (ko có gọi server)
        var vFilterData = filterOrder(vOrderFilterData);
        // 4 hiện thị dữ liệu
        loadDataToTable(vFilterData);
        $("#table-data_info").html("Tìm thấy " + vFilterData.length + " đối tượng thỏa điều kiện.")

    }

    //Hàm xử lý nút Save data
    function onBtnSaveOrderClick() {
        //B0: Khai báo đối tượng lưu dữ liệu
        var vNewOrderObj = {
            id: 0,
            orderId: "",
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            soLuongNuoc: 0,
            thanhTien: 0,
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            hoTen: "",
            email: 0,
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",
            trangThai: "",
            giamGia: 0
        }
        //B1: Thu thập dữ liệu
        getOrderData(vNewOrderObj);
        //B2: Kiểm tra dữ liệu
        var vIsValidate = validateOrderData(vNewOrderObj);
        if (vIsValidate) {
            //B3: xử lý dữ liệu
            if (gCURRENT_STATUS == 1) { //Thêm mới
                //Kiểm tra mã giảm giá
                var bDiscout = 0;
                $.ajax({
                    type: 'GET',
                    url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vNewOrderObj.idVourcher,
                    contentType: 'application/json;charset=UTF-8',
                    dataType: "json",
                    async: false,
                    success: function (res) {
                        console.log(res);
                        bDiscout = res.phanTramGiamGia;
                    }
                });
                displayOrderInfo(vNewOrderObj, bDiscout);
                $("#modal-btn-confirm-order").on("click", function () {
                    $("#model-create-order").modal("hide");
                    //gọi API lưu dữ liệu
                    callAPIPostToServer(vNewOrderObj);
                });
            }
            if (gCURRENT_STATUS == 2) { //chỉnh sửa
                callAPIPutToServer(vNewOrderObj.id, vNewOrderObj.trangThai);
            }
            gOrderObj.addNewOrder(vNewOrderObj);
            //B4: hiển thị dữ liệu
            loadDataToTable(gOrderObj.orderArr);
            $("#order-modal").modal("hide");
            

        }
    }
    //Hàm fill data to form khi chọn commo size
    function onSelectComboChange() {
        //Reset data
        $("#inp-duongkinh").val("");
        $("#inp-suonnuong").val("");
        $("#inp-salad").val("");
        $("#inp-sluong-nuoc").val("");
        $("#inp-thanhtien").val("");
        var vComboSize = $("#sel-combosize").val(); //lấy giá trị select được chọn
        for (var bI in gCOMBO_SIZE) {
            if (vComboSize === gCOMBO_SIZE[bI].combosize) {
                $("#inp-duongkinh").val(gCOMBO_SIZE[bI].duongKinh);
                $("#inp-suonnuong").val(gCOMBO_SIZE[bI].suonNuong);
                $("#inp-salad").val(gCOMBO_SIZE[bI].saLad);
                $("#inp-sluong-nuoc").val(gCOMBO_SIZE[bI].nuocUong);
                $("#inp-thanhtien").val(gCOMBO_SIZE[bI].thanhTien);
            }
        }
    }
    //Hàm xử lý xóa đơn hàng
    function onIconDeleteUserClick() {
        //B0: khai báo biến (bỏ qua)
        //B1: thu thập dữ liệu (bỏ qua)
        //B2: kiểm tra dữ liệu (bỏ qua)
        //B3: xử lý dữ liệu (delete)
        gOrderObj.deleteOrderData();//Xóa dữ liệu local
        callAPIDeleteOrder(gOrderId);//xóa dữ liệu server
        //B4: hiển thị kết quả
        loadDataToTable(gOrderObj.orderArr);
        //reset dữ liệu
        resetFormData();
        //ẩn modal form
        $("#delete-confirm-modal").modal('hide');
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

    //Hàm lấy dữ liệu người dùng chọn từ form
    function getFormData(paramFilterData) {
        paramFilterData.pizzaType = $("#select-pizzatype").val();
        paramFilterData.status = $("#select-status").val();
    }

    //hàm truyền dữ liệu vào bảng
    function loadDataToTable(paramUserObj) {
        gSTT = 1;
        gTableData.clear();
        gTableData.rows.add(paramUserObj).draw();
    }

    //Hàm lọc danh sách đơn hàng
    function filterOrder(paramFilterObj) {
        var vOrder = [];
        // cần thực hiện trả lại 01 array để display trong bảng
        for (var bI = 0; bI < gOrderObj.orderArr.length; bI++) {
            if (gOrderObj.orderArr[bI].loaiPizza !== null && gOrderObj.orderArr[bI].trangThai !== null)
                if ((paramFilterObj.pizzaType.toLowerCase() == gOrderObj.orderArr[bI].loaiPizza.toLowerCase() || paramFilterObj.pizzaType == 0)
                    && (paramFilterObj.status.toLowerCase() == gOrderObj.orderArr[bI].trangThai.toLowerCase() || paramFilterObj.status == 0)) {
                    vOrder.push(gOrderObj.orderArr[bI]);
                }
        }
        return vOrder;

    }

    //Đặt lại trạng thái ban đầu
    function resetFormData() {
        gCURRENT_STATUS = gDEFAULT_STATUS;
        //reset select option
        $("#sel-combosize").html("").attr("disabled", false);;
        $("#sel-pizzatype")
            .html(`<option value="0">-----Chọn loại Pizza-----</option>
                <option value="Seafood">Hải sản</option>
                <option value="Hawaii">Hawaii</option>
                <option value="Bacon">Thịt hun khói</option>`)
            .attr("disabled", false);;
        $("#sel-drink").html("").attr("disabled", false);;
        $("#modal-sel-status")
            .html(` <option value="open">Open</option>
                <option value="confirmed">Comfirmed</option>
                <option value="cancel">Cancel</option> `)
            .attr("disabled", false);;
        //reset input form
        $("#inp-duongkinh").val("");
        $("#inp-suonnuong").val("");
        $("#inp-salad").val("");
        $("#inp-sluong-nuoc").val("");
        $("#inp-thanhtien").val("");
        $("#inp-voucherid").val("").attr("disabled", false);
        $("#inp-hoten").val("").attr("disabled", false);
        $("#inp-email").val("").attr("disabled", false);
        $("#inp-sodienthoai").val("").attr("disabled", false);
        $("#inp-diachi").val("").attr("disabled", false);
        $("#inp-loinhan").val("").attr("disabled", false);
    }

    //Hàm gọi API lấy danh sách đồ uống
    function callAPIGetDrinks() {
        var vDrinkList = null;
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: 'GET',
            async: false,
            success: function (res) {
                console.log(res);
                vDrinkList = res;

            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });
        return vDrinkList;
    }
    //Hàm load danh sách đồ uống vào select
    function loadDrinkToSelect(paramDrinkMenu) {
        //load drink list to select drint
        $("#sel-drink").append($("<option>", { value: 0, html: "-----Chọn loại nước uống-----" }));
        for (let bI = 0; bI < paramDrinkMenu.length; bI++) {
            $("#sel-drink").append($("<option>", { value: paramDrinkMenu[bI].maNuocUong, html: paramDrinkMenu[bI].maNuocUong }));
        }
    }
    //Hàm load danh sách combo vào select
    function loadComboToSelect() {
        //Load combo size menu to select combo
        $("<option>", { value: 0, html: "-----Chọn Combo size-----" }).appendTo($("#sel-combosize"));
        for (var bI in gCOMBO_SIZE) {
            $("<option>", { value: gCOMBO_SIZE[bI].combosize, html: gCOMBO_SIZE[bI].combosize }).appendTo($("#sel-combosize"));
        }
    }

    //Hàm thu thập dữ liệu
    function getOrderData(paramOrderData) {
        if (gCURRENT_STATUS === gADD_STATUS) {
            paramOrderData.id = gOrderObj.getNewId();
        }
        if (gCURRENT_STATUS == gEDIT_STATUS) {
            paramOrderData.id = gOrderId;
        }
        paramOrderData.kichCo = $("#sel-combosize").find(":selected").val();
        paramOrderData.suon = $("#inp-duongkinh").val().trim();
        paramOrderData.salad = $("#inp-salad").val().trim();
        paramOrderData.soLuongNuoc = $("#inp-sluong-nuoc").val().trim();
        paramOrderData.thanhTien = $("#inp-thanhtien").val().trim();
        paramOrderData.loaiPizza = $("#sel-pizzatype").find(":selected").val();
        paramOrderData.idVourcher = $("#inp-voucherid").val().trim();
        paramOrderData.idLoaiNuocUong = $("#sel-drink").find(":selected").val();
        paramOrderData.hoTen = $("#inp-hoten").val().trim();
        paramOrderData.email = $("#inp-email").val().trim();
        paramOrderData.soDienThoai = $("#inp-sodienthoai").val().trim();
        paramOrderData.diaChi = $("#inp-diachi").val().trim();
        paramOrderData.loiNhan = $("#inp-loinhan").val().trim();
        paramOrderData.trangThai = $("#modal-sel-status").find(":selected").val();

    }
    //Hàm kiểm tra dữ liệu người dùng
    function validateOrderData(paramOrderData) {
        if (paramOrderData.kichCo == 0) {
            alert("Please select Combo size!");
            return false;
        }
        if (paramOrderData.loaiPizza == 0) {
            alert("Please select Pizza type!");
            return false;
        }
        if (isNaN(paramOrderData.idVourcher) && paramOrderData.idVourcher !== "") {
            alert("ID Voucher must be a number!");
            return false;
        }
        if (paramOrderData.idLoaiNuocUong == 0) {
            alert("Please select a drink type!");
            return false;
        }
        if (paramOrderData.hoTen === "") {
            alert("Please input full name!");
            return false;
        }
        if (paramOrderData.diaChi === "") {
            alert("Please input address!");
            return false;
        }
        if (paramOrderData.soDienThoai === "") {
            alert("Please input number phone!");
            return false;
        }
        var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!(re.test(paramOrderData.email)) && paramOrderData.email !== "") {
            alert("Format email is invalid!");
            return false;
        }
        if (paramOrderData.trangThai == 0) {
            alert("Please input last name!");
            return false;
        }
        return true;
    }
    //Hàm hiển thị xác nhận thông tin đơn hàng
    function displayOrderInfo(paramOrder, paramDiscount) {
        // debugger;
        $("#model-create-order").modal("show");
        var vTextArea = $("#model-create-order").find(".modal-body").children("p");
        var vTextContent = "Thông tin đơn hàng: </br>";
        vTextContent += "Mã đơn hàng: " + paramOrder.orderId + "</br>";
        vTextContent += "Đơn giá: " + paramOrder.thanhTien + " VNĐ</br>";
        var vDisCount = parseInt(paramDiscount);
        var vPrice = 0;
        if (vDisCount == 0) {
            vPrice = parseInt(paramOrder.thanhTien);
        }
        else
            vPrice = (parseInt(paramOrder.thanhTien) - parseInt(paramOrder.thanhTien) * vDisCount / 100);

        vTextContent += "Phần trăm giảm giá: " + vDisCount + " %</br>";
        vTextContent += "Thành tiền: " + vPrice + " VNĐ";
        vTextArea.html(vTextContent);

    }
    //Hàm gọi API đẩy dữ liệu lên server - thêm mới
    function callAPIPostToServer(paramOrderObj) {
        $.ajax({
            type: 'POST',
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders',
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(paramOrderObj),
            dataType: "json",
            success: function (res) {
                alert("Thêm đơn hàng thành công!");
                location.reload();

            },
            error: function (ajaxContext) {
                alert("Thêm đơn hàng bị lỗi!");
            }
        });

    }
    //Hàm gọi API đẩy dữ liệu lên server - chỉnh sửa
    function callAPIPutToServer(paramOrderid, paramStatus) {
        var vObjectRequest = {
            trangThai: paramStatus //3 trang thai open, confirmed, cancel
        }
        $.ajax({
            type: 'PUT',
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders/' + paramOrderid,
            contentType: 'application/json;charset=UTF-8',
            data: JSON.stringify(vObjectRequest),
            dataType: "json",
            success: function (res) {
                alert("Cập nhật đơn hàng thành công!");
                location.reload();

            },
            error: function (ajaxContext) {
                alert("Cập nhật đơn hàng bị lỗi. Vui lòng thử lại!");
            }
        });
    }
    //Hàm gọi API xóa đơn hàng
    function callAPIDeleteOrder() {
        $.ajax({
            type: 'DELETE',
            url: 'http://42.115.221.44:8080/devcamp-pizza365/orders/' + gOrderId,
            contentType: 'application/json;charset=UTF-8',
            dataType: "json",
            success: function (res) {
                alert("Xóa đơn hàng thành công!");

            },
            error: function (ajaxContext) {
                alert("Xóa đơn hàng bị lỗi. Vui lòng thử lại!");
            }
        });
    }
});


